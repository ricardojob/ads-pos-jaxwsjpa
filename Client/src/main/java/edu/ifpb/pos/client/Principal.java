package edu.ifpb.pos.client;

import edu.ifpb.pos.Autor;
import edu.ifpb.pos.Cadastro;
import edu.ifpb.pos.CadastroAutorWS; 
import edu.ifpb.pos.Livro;

/**
 *
 * @author Ricardo Job
 */
public class Principal {

    public static void main(String[] args) {
        Cadastro endpoint = new Cadastro();
        CadastroAutorWS service = endpoint.getCadastroAutorWSPort();

        Autor autor = new Autor();
        autor.setNome("Kiko");

        Livro livro = new Livro();
        livro.setAutor(autor);
        livro.setPreco(61.5d);
        livro.setTitulo("Meu Tesouro Encantado");

//       service.salvar(livro);
        
       for (Livro book : service.listarLivros()) {
            System.out.print("Livro: " + book.getTitulo());
            System.out.println("\tAutor: " + book.getAutor().getNome());
        }

    }
}
