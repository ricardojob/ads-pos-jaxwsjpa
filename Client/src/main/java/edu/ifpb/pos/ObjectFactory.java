
package edu.ifpb.pos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the edu.ifpb.pos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Salvar_QNAME = new QName("http://ws.pos.ifpb.edu/", "salvar");
    private final static QName _ListarAutoresResponse_QNAME = new QName("http://ws.pos.ifpb.edu/", "listarAutoresResponse");
    private final static QName _ListarAutores_QNAME = new QName("http://ws.pos.ifpb.edu/", "listarAutores");
    private final static QName _Livro_QNAME = new QName("http://ws.pos.ifpb.edu/", "livro");
    private final static QName _ListarLivrosResponse_QNAME = new QName("http://ws.pos.ifpb.edu/", "listarLivrosResponse");
    private final static QName _ListarLivros_QNAME = new QName("http://ws.pos.ifpb.edu/", "listarLivros");
    private final static QName _SalvarResponse_QNAME = new QName("http://ws.pos.ifpb.edu/", "salvarResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: edu.ifpb.pos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SalvarResponse }
     * 
     */
    public SalvarResponse createSalvarResponse() {
        return new SalvarResponse();
    }

    /**
     * Create an instance of {@link ListarLivros }
     * 
     */
    public ListarLivros createListarLivros() {
        return new ListarLivros();
    }

    /**
     * Create an instance of {@link ListarLivrosResponse }
     * 
     */
    public ListarLivrosResponse createListarLivrosResponse() {
        return new ListarLivrosResponse();
    }

    /**
     * Create an instance of {@link Livro }
     * 
     */
    public Livro createLivro() {
        return new Livro();
    }

    /**
     * Create an instance of {@link ListarAutores }
     * 
     */
    public ListarAutores createListarAutores() {
        return new ListarAutores();
    }

    /**
     * Create an instance of {@link ListarAutoresResponse }
     * 
     */
    public ListarAutoresResponse createListarAutoresResponse() {
        return new ListarAutoresResponse();
    }

    /**
     * Create an instance of {@link Salvar }
     * 
     */
    public Salvar createSalvar() {
        return new Salvar();
    }

    /**
     * Create an instance of {@link Autor }
     * 
     */
    public Autor createAutor() {
        return new Autor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Salvar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "salvar")
    public JAXBElement<Salvar> createSalvar(Salvar value) {
        return new JAXBElement<Salvar>(_Salvar_QNAME, Salvar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAutoresResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "listarAutoresResponse")
    public JAXBElement<ListarAutoresResponse> createListarAutoresResponse(ListarAutoresResponse value) {
        return new JAXBElement<ListarAutoresResponse>(_ListarAutoresResponse_QNAME, ListarAutoresResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarAutores }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "listarAutores")
    public JAXBElement<ListarAutores> createListarAutores(ListarAutores value) {
        return new JAXBElement<ListarAutores>(_ListarAutores_QNAME, ListarAutores.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Livro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "livro")
    public JAXBElement<Livro> createLivro(Livro value) {
        return new JAXBElement<Livro>(_Livro_QNAME, Livro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarLivrosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "listarLivrosResponse")
    public JAXBElement<ListarLivrosResponse> createListarLivrosResponse(ListarLivrosResponse value) {
        return new JAXBElement<ListarLivrosResponse>(_ListarLivrosResponse_QNAME, ListarLivrosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarLivros }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "listarLivros")
    public JAXBElement<ListarLivros> createListarLivros(ListarLivros value) {
        return new JAXBElement<ListarLivros>(_ListarLivros_QNAME, ListarLivros.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalvarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.pos.ifpb.edu/", name = "salvarResponse")
    public JAXBElement<SalvarResponse> createSalvarResponse(SalvarResponse value) {
        return new JAXBElement<SalvarResponse>(_SalvarResponse_QNAME, SalvarResponse.class, null, value);
    }

}
