/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ws;

import edu.ifpb.pos.Autor;
import edu.ifpb.pos.Livro;
import edu.ifpb.pos.ejb.LivroFacadeRemote;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Ricardo Job
 */
@WebService(serviceName = "Cadastro")
public class CadastroAutorWS implements Cadastro {

    @EJB
    private LivroFacadeRemote facadeLivro;

    @Override
    @WebMethod(operationName = "salvar")
    public void salvar(Livro livro) {
        facadeLivro.create(livro);
    }

    @Override
    @WebMethod(operationName = "listarLivros")
    public List<Livro> listarLivros() {

        Autor autor = new Autor();
        autor.setId(1);
        autor.setNome("Kiko");

        Livro livro = new Livro();
        livro.setId(9);
        livro.setAutor(autor);
        livro.setPreco(61.5d);
        livro.setTitulo("Vila do Chaves ");

        Autor autor2 = new Autor();
        autor2.setId(2);
        autor2.setNome("Seu Madruga");

        Livro livro2 = new Livro();
        livro2.setAutor(autor2);
        livro2.setPreco(61.5d);
        livro2.setTitulo("Vila do 71");

        List<Livro> livros = new ArrayList<>();
        livros.add(livro);
        livros.add(livro2);

        //return livros;
          return facadeLivro.findAll();//create(livro);
    }

}
