/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ws;

import edu.ifpb.pos.Livro;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Ricardo Job
 */
@WebService
public interface Cadastro {

    @WebMethod(operationName = "salvar")
    public void salvar(Livro livro);

    @WebMethod(operationName = "listarLivros")
    public List<Livro> listarLivros();
}
