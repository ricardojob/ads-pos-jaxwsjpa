/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ejb;

import edu.ifpb.pos.Livro;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Ricardo Job
 */
@Remote
public interface LivroFacadeRemote {

    public void create(Livro entity);

    public void edit(Livro entity);

    public List<Livro> findAll();

    public void remove(Livro entity);

}
