/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ejb;

import edu.ifpb.pos.Autor;
import edu.ifpb.pos.Livro;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ricardo Job
 */
@Local
public interface LivroFacadeLocal {

    public void create(Livro entity);

    public void edit(Livro entity);

    public List<Livro> findAll();

    public List<Autor> findAllAutor();

    public void remove(Livro entity);

}
