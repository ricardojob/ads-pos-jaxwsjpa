/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ejb;

import edu.ifpb.pos.Autor;
import edu.ifpb.pos.Livro;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ricardo Job
 */
@Stateless
public class LivroFacade implements LivroFacadeLocal {

    @PersistenceContext(unitName = "Cadastro")
    private EntityManager em;

    public void create(Livro entity) {
        em.persist(entity);
    }

    public void edit(Livro entity) {
        em.merge(entity);
    }

    public void remove(Livro entity) {
        em.remove(em.merge(entity));
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Livro> findAll() {
        Query query = em.createQuery("Select l From Livro l");
        List<Livro> retorno = query.getResultList();
        return retorno;

    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Autor> findAllAutor() {
        Query query = em.createQuery("Select a From Autor a");
        List<Autor> retorno = query.getResultList();
        return retorno;

    }

}
