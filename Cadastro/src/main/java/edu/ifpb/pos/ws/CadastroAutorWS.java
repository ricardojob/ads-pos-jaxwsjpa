/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.ws;

import edu.ifpb.pos.Autor;
import edu.ifpb.pos.Livro;
import edu.ifpb.pos.ejb.LivroFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Ricardo Job
 */
@WebService(serviceName = "Cadastro")
@Stateless
public class CadastroAutorWS implements Cadastro {

    //Essa interface deve ser Local, para ser injetada corretamente
    @EJB
    private LivroFacadeLocal facadeLivro;

    @Override
    @WebMethod(operationName = "salvar")
    public void salvar(Livro livro) {
        facadeLivro.create(livro);
    }

    @Override
    @WebMethod(operationName = "listarAutores")
    public Autor[] listarAutores() {
        List<Autor> autores = facadeLivro.findAllAutor();
        return autores.toArray(new Autor[autores.size()]);
    }

    @Override
    @WebMethod(operationName = "listarLivros")
    public Livro[] listarLivros() {
        List<Livro> livros = facadeLivro.findAll();
        return livros.toArray(new Livro[livros.size()]);
    }

}
